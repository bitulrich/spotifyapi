<?php 
session_start();
require '../vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
//use \Actions;
use Servicios\Actions\SpotifyController;
use Servicios\Actions\Callback;
use Middlewares\AlbumsMiddleware;
use Middlewares\CallbackMiddleware;
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
        'modules' => [
            'local' => './../config/local/modulos.php'
        ]
    ],
];
$c = new \Slim\Container($configuration);



$app = new \Slim\App($c);

$app->get('/api/v1/albums',SpotifyController::class)->add(AlbumsMiddleware::class);
$app->get('/callback',Callback::class)->add(CallbackMiddleware::class);
$app->run();
