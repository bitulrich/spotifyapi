<?php 
namespace Servicios\Actions;

use Exception;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Servicios\Models\SpotifyService;

class SpotifyController extends Action{
    private $spotifyService;
    public function __construct()
    {
        $this->spotifyService = new SpotifyService();
    }
    public function __invoke(Request $request, Response $response, $args = [])
    {
        try{
            $artist = isset($request->getQueryParams()['q'])?$request->getQueryParams()['q'] : null;
            if(!$artist){
                throw new Exception("Debe ingresar un artista",405);
            }
            $albums = $this->spotifyService->getAlbums($artist);
            if(count($albums['albums']['items'])< 1){
                throw new Exception("No hay regitros sobre el artista seleccionado",404);
            }
            $respuesta = array();
            $i=0;
            foreach($albums['albums']['items'] as $album){
                $respuesta[$i]['name'] = $album['name'];
                $respuesta[$i]['released'] = $album['release_date'];
                $respuesta[$i]['tracks'] = $album['total_tracks'];
                $respuesta[$i]['cover'] = $album['images'];
                $i++;
            }

            return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($respuesta));
        }catch(\Exception $e){
            return $response->withJson($e->getMessage(),$e->getCode());
        }
    }
}