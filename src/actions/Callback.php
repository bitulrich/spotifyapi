<?php 
namespace Servicios\Actions;

use Servicios\Models\SpotifyService;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Callback extends Action{
    public function __invoke(Request $request, Response $response, $args = [])
    {
        if(!isset($_SESSION['artist'])){
            return  $response->withJson(['error' => 'Debe igresar un artista'],404);
        }else{
            return $response->withStatus(301)->withHeader('Location','/api/v1/albums?q='.$_SESSION['artist']);  
        }
    }
}