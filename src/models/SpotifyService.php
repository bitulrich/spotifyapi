<?php

namespace Servicios\Models;

use Modulos\APIClient;

class SpotifyService{
    public function __construct(){
       
    }

    public function getUrlToken(){
        APIClient::initialize('dev');
        return APIClient::getUrlToken();
    }

    public function getToken($code){
        APIClient::initialize('dev');
        APIClient::getToken($code);
    }

    public function refreshToken(){
        APIClient::initialize('dev');
        APIClient::refreshToken();
    }

    public function getAlbums($artist){
        try{
            APIClient::initialize('dev');
            return APIClient::getArtistAlbums($artist);
        }catch(\Exception $e){
            throw $e;
        }
    }

}