<?php

/*$base_path = 'https://accounts.spotify.com/authorize';
$client_id = '65cb1a4981dd4f3eb433bf18b9a3d803';
$response_type = 'code';
$redirect_uri = 'http://aivo.com/sign_in.php';*/

namespace Modulos;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;


abstract class APIClient
{
    private static $config;
    private static $base_path;
    private static $response_type;
    private static $redirect_uri;
    private static $client_id;
    private static $token_base_path;
    private static $client;
    private static $grant_type;
    private static $client_secret;
    private static $search_url;

    private function __construct()
    {
    }

    public static function initialize($env)
    {
        if (!file_exists(__DIR__ . '/configModulos.ini')) {
            throw new \Exception("[ApiClient] No se encontró el archivo configModulos.ini",500);
        }
        $iniConfig = parse_ini_file(__DIR__ . '\configModulos.ini', true);
        if (!$iniConfig) {
            throw new \Exception("[ApiClient] El archivo 'configModulos.ini'  no pudo ser analizado",500);
        }
        if (!isset($iniConfig[$env])) {
            throw new \Exception("[ApiClient] El archivo 'configModulos.ini'  no pudo ser analizado. Falta definir el entorno de ejecución",500);
        }
        if (!isset($iniConfig[$env]['base_path'])) {
            throw new \Exception("[ApiClient] El archivo 'configModulos.ini'  no pudo ser analizado. Falta definir el entorno de ejecución",500);
        }
        if (!isset($iniConfig[$env]['client_id'])) {
            throw new \Exception("[ApiClient] El archivo 'configModulos.ini'  no pudo ser analizado. Falta definir el access token",500);
        }
        if (!isset($iniConfig[$env]['response_type'])) {
            throw new \Exception("[ApiClient] El archivoEl archivo 'configModulos.ini'  no pudo ser analizado. Falta definir el access token",500);
        }
        if (!isset($iniConfig[$env]['redirect_uri'])) {
            throw new \Exception("[ApiClient] El archivo 'configModulos.ini'  no pudo ser analizado. Falta definir el access token",500);
        }
        if (!isset($iniConfig[$env]['token_base_path'])) {
            throw new \Exception("[ApiClient] El archivo 'configModulos.ini'  no pudo ser analizado. Falta definir el base path del token",500);
        }
        if (!isset($iniConfig[$env]['grant_type'])) {
            throw new \Exception("[ApiClient] El archivo 'configModulos.ini'  no pudo ser analizado. Falta definir el grant_type para obtener el token",500);
        }
        if (!isset($iniConfig[$env]['client_secret'])) {
            throw new \Exception("[ApiClient] El archivo 'configModulos.ini'  no pudo ser analizado. Falta definir el client_secret para obtener el token",500);
        }
        if (!isset($iniConfig[$env]['search_url'])) {
            throw new \Exception("[ApiClient] El archivo 'configModulos.ini'  no pudo ser analizado. Falta definir la url de búsqueda para obtener los albums",500);
        }
        self::$client = new Client();
        self::$base_path = $iniConfig[$env]['base_path'];
        self::$client_id = $iniConfig[$env]['client_id'];
        self::$redirect_uri = $iniConfig[$env]['redirect_uri'];
        self::$response_type = $iniConfig[$env]['response_type'];
        self::$token_base_path = $iniConfig[$env]['token_base_path'];
        self::$grant_type = $iniConfig[$env]['grant_type'];
        self::$client_secret = $iniConfig[$env]['client_secret'];
        self::$search_url = $iniConfig[$env]['search_url'];
    }

    public static function getUrlToken()
    {
        $data = http_build_query(array(
            'client_id' => self::$client_id,
            'response_type' => self::$response_type,
            'redirect_uri' => self::$redirect_uri
        ));
        $url = self::$base_path . '?' . $data;
        return $url;
    }

    public static function getToken($code){
        self::getTokenSpotify($code);
    }

    public static function refreshToken(){
        $code = $_SESSION['refresh_token'];
        self::getTokenSpotify($code);
    }

    private static function getTokenSpotify($code){
        try{
            $response = self::$client->request('POST',self::$token_base_path,[
                'headers' => [
                    "Authorization" => 'Basic ' . base64_encode(self::$client_id.':'.self::$client_secret) 
                ],
                'form_params' => [
                    'grant_type' => self::$grant_type,
                    'code' => $code,
                    'redirect_uri' => self::$redirect_uri
                ]
            ]);
            $token = json_decode($response->getBody(),true);
            if($token){
                $_SESSION['access_token'] = $token['access_token'];
                $_SESSION['refresh_token'] = $token['refresh_token'];
                $_SESSION['expires_in'] = $token['expires_in'];
            }
        }catch(\GuzzleHttp\Exception\ClientException $e){
            $response = $e->getResponse();
            $error =json_decode($response->getBody()->getContents(),true); 
            if(isset($error['error']['invalid_grant'])){
                unset($_SESSION['access_token']);
                unset($_SESSION['refresh_token']);
                unset($_SESSION['expires_in']);
            }
            header('Location: ' .  self::getUrlToken());
            exit;
        }
        
    }

    public static function getArtistAlbums($artist){
        try{
            $response = self::$client->request('GET',self::$search_url."?q=artist:$artist&type=album",[
                'headers' => [
                    "Authorization" => 'Bearer ' .  $_SESSION['access_token'],
                    "Content-Type" => "application/x-www-form-urlencoded"
                ]
            ]);
            $albums = json_decode($response->getBody(),true);
            
            return $albums;
        }catch(\GuzzleHttp\Exception\ClientException $e){
            $response = $e->getResponse();
            $error =json_decode($response->getBody()->getContents(),true); 
            if($error['error']['status'] == 401){
                self::refreshToken();
                self::getArtistAlbums($artist);
            }else{
                header('Location: ' .  self::getUrlToken());
                exit;
            }
        }catch(\Exception $e){
            throw $e;
        }
    }
}
