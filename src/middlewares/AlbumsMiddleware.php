<?php 
namespace Middlewares;
use Servicios\Models;
use Servicios\Models\SpotifyService;

class AlbumsMiddleware{
    private $urlLogin;

    public function __construct() {
    }
    public function __invoke($request,$response,$next)
    {
       $spotifyService = new SpotifyService();
       if(!isset($_SESSION['access_token'])){
            $_SESSION['artist'] = $request->getQueryParams()['q'];
            $this->urlLogin = $spotifyService->getUrlToken();
            http_response_code(303);
            header('Location: ' .  $this->urlLogin);
            exit;
            
       }else{
        $response = $next($request, $response);
        return $response;
       }
       

    }
}