<?php 
namespace Middlewares;
use Servicios\Models;
use Servicios\Models\SpotifyService;

class CallbackMiddleware{
    private $urlLogin;

    public function __construct() {
    }
    public function __invoke($request,$response,$next)
    {
       $spotifyService = new SpotifyService();
       $code = null;
       $code = $request->getQueryParams()['code'];
       if(!$code){
        $_SESSION['artist'] = $request->getQueryParams()['q'];
        $this->urlLogin = $spotifyService->getUrlToken();
        http_response_code(303);
        header('Location: ' .  $this->urlLogin);
        exit;
       }
       $spotifyService->getToken($code);
       $response = $next($request, $response);
        return $response;
    }
}